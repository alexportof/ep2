# Exercício Programado 2

## Introdução

No mundo de Pokémon, existem criaturas chamadas Pokémons. Estas criaturas possuem
diversas caracterísiticas e atributos.

Os Pokémons se organizam em tipos, e podem ser capturados por treinadores.

Neste Exercício Programado, você irá criar a sua Pokédex, uma aplicação para
visualizar informações dos Pokémons que cada treinador possui.

## Objetivo

Construir uma Pokédex simples. Onde os treinadores possam se cadastrar,
e cadastrar os seus Pokémons. E possam ver as características destes Pokémons.

## IDE utilizada

A IDE escolhida foi o NetBeans

##  Passo a Passo

na tela de login,colocar nome e senha
depois,pode escolher se quer cadastrar mais um treinador ou escolher os pokemons
por conta do tempo,so consegui colocar os pokemons de 2 classes a Fairy e Steel
depois clicar no botao atualizar e terá a lista completa de pokemons de cada tipo escolhido
coloque o nome do pokemon que quer adicionar ao treinador e na outra tela colocar o nome do Treinador

pelo nome,pode colocar qualquer pokemon e depois clicar em pesquisar
e colocar o nome do Treinador que deseja atribuir o pokemon

para ver os pokemons que um treinador possui,clicar em Ver Treinadores
e depois digitar o nome do treinador ja cadastrado,clicar em atualizar e mostrará cada pokemon deste treinador

