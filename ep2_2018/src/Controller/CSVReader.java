
package Controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class CSVReader {
    
    public void CSVReader1(String url, PokemonController pokemonController)  throws FileNotFoundException, IOException {
        String csvSeparator = ",";
        BufferedReader csvContent = null;
        String line = "";        
         try{
            csvContent = new BufferedReader(new FileReader(url));
            while((line = csvContent.readLine()) != null){
                String[] pokemon = line.split(csvSeparator);
                String name = pokemon[0];
                String type = pokemon[1];
                String type2 = pokemon[2];
                int hp = Integer.parseInt(pokemon[3]);
                int attack = Integer.parseInt(pokemon[4]);
                int defense = Integer.parseInt(pokemon[5]); 
                pokemonController.createPokemons(name, type, type2, hp, attack, defense);
            }
        } catch(FileNotFoundException e){
            System.out.println("File not Found File 1");
        } finally {
            if(csvContent != null){
                try{
                    csvContent.close();
                } catch(IOException e){
                    System.out.println("IO error file 1");
                }catch(ArrayIndexOutOfBoundsException e){
        }
            }
        }
    }
     public void CSVReader2(String url,  PokemonController pokemonController)  throws FileNotFoundException, IOException {
        String csvSeparator = ",";
        BufferedReader csvContent2 = null;
        String line = "";
        int i = 0;
        try{
            csvContent2 = new BufferedReader(new FileReader(url));
            while((line = csvContent2.readLine()) != null){
                String[] pokemon2 = line.split(csvSeparator);
                int experience = Integer.parseInt(pokemon2[0]);
                String ability = pokemon2[1];
                String ability2 = pokemon2[2];
                String ability3 = pokemon2[3];
                pokemonController.createPokemons(i, experience, ability, ability2, ability3);
                i++;
            }
        } catch(FileNotFoundException e){
            System.out.println("File not Found File 2");
        }catch(ArrayIndexOutOfBoundsException e){

        }catch(IOException e){
            System.out.println("IO error");
        }finally {
            if(csvContent2 != null){
                try{
                    csvContent2.close();
                } catch(IOException e){
                    System.out.println("IO error file 2");
                }
            }
        }
     }
}
