package Controller;

import View.FairyType;
import View.LoginScreen;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Pokedex {
    public static void main(String[] args) throws FileNotFoundException, IOException{
        String csvFile1 = "./data/data_csv_files_POKEMONS_DATA_1.csv";
        String csvFile2 = "./data/data_csv_files_POKEMONS_DATA_2.csv";
        CSVReader reader = new CSVReader();
          PokemonController pokemonController = new PokemonController();
          reader.CSVReader1(csvFile1, pokemonController);
          reader.CSVReader2(csvFile2, pokemonController );
           pokemonController.separatesPokemons();
          
        LoginScreen login = new LoginScreen();
        login.setVisible(true);

   
    }
}
