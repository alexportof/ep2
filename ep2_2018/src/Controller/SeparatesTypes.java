
package Controller;

import Model.Pokemon;
import Model.PokemonsTypes;
import View.LoginScreen;
import View.PokedexView;
import java.util.ArrayList;

public class SeparatesTypes {       
   
    public void separe(Pokemon pokemon){
       if(pokemon.getType().equals("Steel")  || pokemon.getType2().equals("Steel")){
            PokemonsTypes.getInstance().addPokemons(pokemon,"steel");
            }
        else if(pokemon.getType().equals("Fairy")  || pokemon.getType2().equals("Fairy")){
               PokemonsTypes.getInstance().addPokemons(pokemon,"fairy");
            }
        else if(pokemon.getType().equals("Dark")  || pokemon.getType2().equals("Dark")){
               PokemonsTypes.getInstance().addPokemons(pokemon,"dark");
            }
        else if(pokemon.getType().equals("Dragon")  || pokemon.getType2().equals("Dragon")){
                PokemonsTypes.getInstance().addPokemons(pokemon,"dragon");
            }
        else if(pokemon.getType().equals("Ghost")  || pokemon.getType2().equals("Ghost")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"ghost");
            }
        else if(pokemon.getType().equals("Water")  || pokemon.getType2().equals("Water")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"water");
            }
        else if(pokemon.getType().equals("Bug")  || pokemon.getType2().equals("Bug")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"bug");
            }
        else if(pokemon.getType().equals("Psychic")  || pokemon.getType2().equals("Psychic")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"psychic");
            }
        else if(pokemon.getType().equals("Flying")  || pokemon.getType2().equals("Flying")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"flying");
            }
        else if(pokemon.getType().equals("Ground")  || pokemon.getType2().equals("Ground")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"ground");
            }
        else if(pokemon.getType().equals("Fire")  || pokemon.getType2().equals("Fire")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"fire");
            }
        else if(pokemon.getType().equals("Poison")  || pokemon.getType2().equals("Poison")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"poison");
            }
        else if(pokemon.getType().equals("Fighting")  || pokemon.getType2().equals("Fighting")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"fighting");
            }
        else if(pokemon.getType().equals("Ice")  || pokemon.getType2().equals("Ice")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"ice");
            }
        else if(pokemon.getType().equals("Eletric")  || pokemon.getType2().equals("Eletric")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"eletric");
            }
        else if(pokemon.getType().equals("Grass")  || pokemon.getType2().equals("Grass")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"grass");
            }
        else if(pokemon.getType().equals("Normal")  || pokemon.getType2().equals("Normal")){
                 PokemonsTypes.getInstance().addPokemons(pokemon,"normal");
            }        
   }
    
   
}
