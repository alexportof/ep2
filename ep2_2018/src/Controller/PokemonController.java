
package Controller;

import Model.Pokemon;
import View.LoginScreen;
import View.SearchPokemonsTypesView;
import java.util.ArrayList;
import java.util.List;

public class PokemonController {
    private Pokemon model;
    private SearchPokemonsTypesView view;
    private List<Pokemon> pokemons;
    
    public PokemonController(){
       pokemons = new ArrayList<Pokemon>();

    }
    
    public PokemonController(Pokemon model,SearchPokemonsTypesView view){
        this.model = model;
        this.view = view;
    }
    public void createPokemons(String name, String type, String type2, int hp, int attack, int defense){ //polymorphism
        Pokemon pokemon = new Pokemon();
        pokemon.setName(name);
        pokemon.setType(type);
        pokemon.setType2(type2);
        pokemon.setHp(hp);
        pokemon.setAttack(attack);
        pokemon.setDefense(defense);
        this.pokemons.add(pokemon);

    }
public void createPokemons(int i,int experience, String ability, String ability2, String ability3 ){ // polymorphism
    pokemons.get(i).setExperience(experience);
    pokemons.get(i).setAbility(ability);
    pokemons.get(i).setAbility2(ability2);
    pokemons.get(i).setAbility3(ability3);
}
public void separatesPokemons(){

    SeparatesTypes separatesTypes = new SeparatesTypes();
    
    for(int i=0;i<pokemons.size();i++){
            separatesTypes.separe(pokemons.get(i));            
        }
    
    }
    
  
}
