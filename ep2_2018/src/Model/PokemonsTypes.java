
package Model;

import java.util.ArrayList;
import java.util.List;

public class PokemonsTypes {
     private static PokemonsTypes instancia;
     public List<Pokemon> steel;
     public List<Pokemon> fairy;
     public List<Pokemon> dark;
     public List<Pokemon> dragon;
     public List<Pokemon> ghost;
     public List<Pokemon> water;
     public List<Pokemon> bug;
     public List<Pokemon> psychic;
     public List<Pokemon> flying;
     public List<Pokemon> ground;
     public List<Pokemon> fire;
     public List<Pokemon> poison;
     public List<Pokemon> fighting;
     public List<Pokemon> ice;
     public List<Pokemon> eletric;
     public List<Pokemon> grass;
     public List<Pokemon> normal;
     public List<Pokemon> allPokemons;
     
    public PokemonsTypes(){
         steel = new ArrayList<Pokemon>();
         fairy = new ArrayList<Pokemon>();
         dark = new ArrayList<Pokemon>();
         dragon = new ArrayList<Pokemon>();
         ghost = new ArrayList<Pokemon>();
         water = new ArrayList<Pokemon>();
         bug = new ArrayList<Pokemon>();
         psychic = new ArrayList<Pokemon>();
         flying = new ArrayList<Pokemon>();
         ground = new ArrayList<Pokemon>();
         fire = new ArrayList<Pokemon>();
         poison = new ArrayList<Pokemon>();
         fighting = new ArrayList<Pokemon>();
         ice = new ArrayList<Pokemon>();
         eletric = new ArrayList<Pokemon>();
         grass = new ArrayList<Pokemon>();
         normal = new ArrayList<Pokemon>();
         allPokemons = new ArrayList<Pokemon>();
     
    }
    public static synchronized  PokemonsTypes getInstance(){
        if(instancia == null ){
            instancia = new PokemonsTypes();
        }
        return instancia;
    }
    public void addPokemons(Pokemon pokemon, String type){
       this.allPokemons.add(pokemon);
        if("steel".equals(type)){
             this.steel.add(pokemon);
        }
        else if("fairy".equals(type)){
            this.fairy.add(pokemon);
        }
        else if("dark".equals(type)){
            this.dark.add(pokemon);
        }
        else if("dragon".equals(type)){
            this.dragon.add(pokemon);
        }
        else if("ghost".equals(type)){
            this.ghost.add(pokemon);
        }
        else if("water".equals(type)){
            this.water.add(pokemon);
        }
        else if("bug".equals(type)){
            this.bug.add(pokemon);
        }
        else if("psychic".equals(type)){
            this.psychic.add(pokemon);
        }
        else if("flying".equals(type)){
            this.flying.add(pokemon);
        }
        else if("ground".equals(type)){
            this.ground.add(pokemon);
        }
        else if("fire".equals(type)){
            this.fire.add(pokemon);
        }
        else if("poison".equals(type)){
            this.poison.add(pokemon);
        }
        else if("fighting".equals(type)){
            this.fighting.add(pokemon);
        }
        else if("ice".equals(type)){
            this.ice.add(pokemon);
        }
        else if("eletric".equals(type)){
            this.eletric.add(pokemon);
        }
        else if("grass".equals(type)){
            this.grass.add(pokemon);
        }
        else if("normal".equals(type)){
            this.normal.add(pokemon);
        }
        
    }
    
}
