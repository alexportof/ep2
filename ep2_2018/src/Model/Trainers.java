
package Model;

import java.util.ArrayList;
import java.util.List;


public class Trainers {
    private static Trainers instancia;
    public List<Trainer> trainers;
    
    private Trainers(){
        trainers = new ArrayList<Trainer>();
    }
    public static synchronized  Trainers getInstance(){
        if(instancia == null ){
            instancia = new Trainers();
        }
        return instancia;
    }
    
   public void createAndAddTrainer(String name, String password){
    Trainer trainer = new Trainer();
    trainer.setName(name);
    trainer.setPassword(password);
     this.trainers.add(trainer);
    }
   public void showTrainer(int i){
       System.out.println(trainers.get(i).getName());
       System.out.println(trainers.get(i).getPassword());
   }
   
}
