package Model;

public class Pokemon {
    private String name;
    private String type;
    private String type2; 
    private int hp;
    private int attack;
    private int defense;
    private int experience;
    private String ability;
    private String ability2;
    private String ability3;
    

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    
    public String getType(){
        return type;
    }
    public void  setType(String type){
        this.type = type;
    }
    public String getType2(){
        return type2;
    }
    public void  setType2(String type2){
        this.type2 = type2;
    }
    public int getHp(){
        return hp;
    }
    public void setHp(int hp){
        this.hp = hp;
    }
    public int getAttack(){
        return attack;
    }
    public void setAttack(int attack){
        this.attack = attack;
    }
    public int getDefense(){
        return defense;
    }
    public void setDefense(int defense){
        this.defense = defense;
    }
    public int getExperience(){
        return experience;
    }
    public void setExperience(int experience){
        this.experience = experience;
    }
    
    public String getAbility(){
        return ability;
    }
    public void setAbility(String ability){
        this.ability = ability;
    }
    public String getAbility2(){
        return ability2;
    }
    public void setAbility2(String ability2){
        this.ability2 = ability2;
    }
    public String getAbility3(){
        return ability3;
    }
    public void setAbility3(String ability3){
        this.ability3 = ability3;
    }
    
}    